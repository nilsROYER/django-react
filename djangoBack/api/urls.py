from django.urls import path, include
from .views import RoomViews, CreateRoomView, GetRoom

urlpatterns = [
    path("rooms/", RoomViews.as_view(), name="room"),
    path('room/', GetRoom.as_view()),
    path("room/new", CreateRoomView.as_view()),
]
