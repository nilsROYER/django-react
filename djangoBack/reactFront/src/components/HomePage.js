import React, { Component } from 'react';
import RoomJoinPage from './RoomJoinPage'
import CreateRoomPage from './CreateRoomPage'
import Room from "./Room"
import { BrowserRouter as Router, Switch , Route, Link, Redirect } from 'react-router-dom'

const HomePage = (props) => {
   
        return (
            <Router>
                <Switch>
                    <Route exact path="/room"><p>this is the home page</p></Route>
                    <Route path="/room/join" component={RoomJoinPage} />
                    <Route path="/room/create" component={CreateRoomPage} />
                    <Route path="/room/:roomCode" component={Room} />
                </Switch>
            </Router>
        )
}

export default HomePage